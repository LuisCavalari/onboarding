import Vue from "vue";
import Vuex from "vuex";
import api from "../services/api";
import { setToken , getToken, clearToken } from "../services/token";
import { format,parseISO } from "date-fns";
Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        token:getToken() || '',
        user: {},
    },
    mutations: {
        setUser(state,user) {
            state.user = user
        },
        setToken(state,token) {
            state.token = token
        }
    },
    getters: {
        formatedLastLogin(state) {
            const user = state.user;
            if (!user) return;
            if(!user.last_login) return
            let lastLogin = parseISO(user.last_login)
            const date = format(lastLogin, "d/M/yyyy HH:m");
            return `${date}`;
        },
        getUser(state) {
          return state.user
        },
        isAuthenticated(state) {
            return !!state.token
        }
    },
    actions: {
        logout({ commit }) {
            clearToken()
            commit('setToken','');
            commit('setUser',{})
            
        },
        async registerUser({commit},user) {
            const response = await api.post('/user', user)
            const { user:registeredUser, access_token:token } = response.data
            setToken(token)
            commit('setToken',token)
            commit('setUser',registeredUser)
        },
        async login({commit},credentials) {
            const response = await api.post('/auth/login',{
                email: credentials.email,
                password: credentials.password
            })
            const { access_token:token, user } = response.data
            if(token) setToken(token)
            commit('setUser',user)
            commit('setToken',token)
        },
        async me({commit}) {
            const response = await api.get('/auth/me')
            const user = response.data.user
            commit('setUser',user)
        }
    }

})
export default store