import axios from "axios";
import { getToken } from "./token";
const api = axios.create({
    baseURL: 'https://onboarding-project.herokuapp.com/api'
})

api.interceptors.request.use(async (config)=> {
    const token = getToken()
    const apiCopy = config
    if(token) {
        apiCopy.headers.Authorization = `Bearer ${token}`
    } 
    return apiCopy
})

export default api