export const getToken = ()=> localStorage.getItem('onboarding@token')
export const setToken = token => localStorage.setItem('onboarding@token',token)
export const clearToken = ()=> localStorage.removeItem('onboarding@token')