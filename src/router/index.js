import Vue from 'vue'
import VueRouter from 'vue-router'
import SignUp from '../views/SignUp'
import InitialPage from '../views/InitialPage'
import Login from "../views/Login";
import store from '../store';
import Hobbies from "../views/Hobbies";
import AboutMe from "../views/AboutMe"
import Home from "../views/Home"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Pagina incial',
    component: InitialPage
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/sign-up',
    name: 'Cadastro',
    component: SignUp,
    
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Home,
    meta: {
      requiredAuth: true,
      dashboardLayout: true,
      color:"indigo",
    }
  },
  {
    path:'/hobbies',
    name: 'Hobbies',
    component: Hobbies,
    meta: {
      requiredAuth: true,
      dashboardLayout: true,
      color:"green",
    }
  },
  {
    path:'/about-me',
    name: 'Sobre mim',
    component: AboutMe,
    meta: {
      requiredAuth: true,
      dashboardLayout: true,
      color:"indigo",
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `${to.name} | Onboarding `;
  if (to.matched.some(record => record.meta.requiredAuth)) {
    if (!store.getters.isAuthenticated) {
      next({
        path: '/',
      });
    } else {
      next();
    }
  } else {
    next();
  }
});




export default router
