import Vue from 'vue'
import VMask from "./plugins/Vmask";
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import api from './services/api'
import store from "./store";
import toasted from "./plugins/VToasted";

Vue.config.productionTip = false
Vue.prototype.$http = api
new Vue({
  vuetify,
  router,
  toasted,
  VMask,
  store,
  render: h => h(App)
}).$mount('#app')
