import Vue from "vue";
import VToasted from "vue-toasted";

Vue.use(VToasted, {
    iconPack: 'material',
    position: 'top-right',
    theme: 'toasted-primary',
    duration: 10000
})
Vue.toasted.register(
    'success',
    payload => (!payload.msg ? 'Operação realizada com sucesso!' : payload.msg),
    { type: 'success' },
  );

Vue.toasted.register(
    'error',
    payload => (!payload.msg ? 'Operação realizada com sucesso!' : payload.msg),
    { type: 'error' },
  );

  export default Vue